package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Profile("MyDao")
@Repository
@Slf4j
public class StudentsDaoImpl implements StudentDao{
    List<Student> students;

    public StudentsDaoImpl () {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(4l)
                .studentId("SE-004")
                .name("Dong wook")
                .surname("Lee")
                .gpa(3.21)
                .image("http://popcornfor2.com/upload/111/news-full-110730.jpg")
                .penAmount(9)
                .description("Cute man")
                .build());
        this.students.add(Student.builder()
                .id(5l)
                .studentId("SE-005")
                .name("Bogum")
                .surname("Park")
                .gpa(4.00)
                .image("https://pbs.twimg.com/profile_images/799445590614495232/ii6eBROd.jpg")
                .penAmount(22)
                .description("My boyfriend")
                .build());
        this.students.add(Student.builder()
                .id(6l)
                .studentId("SE-006")
                .name("Myung-soo")
                .surname("Kim")
                .gpa(3.56)
                .image("https://f.ptcdn.info/509/065/000/pwa1101qTVmdf7Q9Ig-o.jpg")
                .penAmount(4)
                .description("Good man")
                .build());
    }

    @Override
    public List<Student> getAllStudent() {
        log.info("My dao is call");
        return students;
    }

    @Override
    public Student findById(Long studentId) {
        return students.get((int) (studentId - 1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}
