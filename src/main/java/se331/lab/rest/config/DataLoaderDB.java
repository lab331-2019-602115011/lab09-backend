package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.StudentRepository;

@Component
public class DataLoaderDB implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        studentRepository.save(Student.builder()
                .id(4l)
                .studentId("SE-004")
                .name("Dong wook")
                .surname("Lee")
                .gpa(3.21)
                .image("http://popcornfor2.com/upload/111/news-full-110730.jpg")
                .penAmount(9)
                .description("Cute man")
                .build());
        studentRepository.save(Student.builder()
                .id(5l)
                .studentId("SE-005")
                .name("Bogum")
                .surname("Park")
                .gpa(4.00)
                .image("https://pbs.twimg.com/profile_images/799445590614495232/ii6eBROd.jpg")
                .penAmount(22)
                .description("My boyfriend")
                .build());
        studentRepository.save(Student.builder()
                .id(6l)
                .studentId("SE-006")
                .name("Myung-soo")
                .surname("Kim")
                .gpa(3.56)
                .image("https://f.ptcdn.info/509/065/000/pwa1101qTVmdf7Q9Ig-o.jpg")
                .penAmount(4)
                .description("Good man")
                .build());
    }
}
